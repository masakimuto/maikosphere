﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Masa.Lib;
using Masa.Lib.XNA;
using Masa.Lib.XNA.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Masa.ScriptEngine;

namespace Masa.Sphere.Chara
{
	public class Ship : CharaBase
	{
		ControlState Input { get { return _.Input.GamePlayControlState; } }


		float Friction { get; set; }
		float YawAngle { get; set; }
		float DefaultAccel { get; set; }
		float VariableAccel { get; set; }

		float AimRadius { get; set; }
		int LockonTime { get; set; }

		Vector2 aimPosition;
		Texture2D aimTexture;

		SkinnedModel.SkinningData skinningData;
		SkinnedModel.AnimationPlayer animePlayer;
		SkinnedModel.AnimationClip animeClip;
		TimeSpan animeTime;
		bool animePlaying;

		Dictionary<Enemy, int> lockon;

		public Ship()
			: base()
		{
			Set();
			SetModel("maiko");
			InitAnime();
			Position = new Vector3(0, SphereRadius, 0);
			Rotation = Vector3.Zero;
			Scale = .005f;
			lockon = new Dictionary<Enemy, int>();
			SetParams();
			aimTexture = _.Game.Content.Load<Texture2D>("texture\\container");
			InitializeVelocity(DefaultAccel, Friction, new Vector3(0, 0, 1));
			UpdateAnime();
		}

		void InitAnime()
		{
			skinningData = Model.Tag as SkinnedModel.SkinningData;
			animePlayer = new SkinnedModel.AnimationPlayer(skinningData);
			animeClip = skinningData.AnimationClips.First().Value;
			animePlayer.StartClip(animeClip);
		}

		void SetParams()
		{
			var script = _.Script.GetScript(this, "ship");
			script.Update();
		}

		void UpdateAnime()
		{
			if (animePlaying)
			{
				animeTime += new TimeSpan(0, 0, 0, 0, 25);
				if (animeTime >= animeClip.Duration)
				{
					animeTime = new TimeSpan();
					animePlaying = false;
				}
			}
			animePlayer.Update(animeTime, false, GetWorld());
		}

		public override void Update()
		{
			base.Update();
			Control();
			Aim();
			UpdateAnime();
		}

		void Aim()
		{
			var state = _.Input.MouseState;
			aimPosition = new Vector2(state.X, state.Y);
			foreach (var item in _.World.Enemys.ActiveItems())
			{
				if((aimPosition - item.ContainerPosition).LengthSquared() < AimRadius * AimRadius)
				{
					Lockon(item);
				}
			}
			UpdateLockon();
			if (state.Left.JustPush)
			{
				Fire();
			}
		}

		void Lockon(Enemy e)
		{
			if (!EnumerateLockonEnemys().Any(x => x.Key == e))
			{
				_.Audio.PlaySound("Lock");
			}
			
			lockon[e] = Count;
		}

		void UpdateLockon()
		{
			_.Audio.PlayLockon(EnumerateLockonEnemys().Any());
			if (EnumerateLockonEnemys().Any(x => Count - x.Value == LockonTime))
			{
				_.Audio.PlaySound("Unlock");
			}
		}

		IEnumerable<KeyValuePair<Enemy, int>> EnumerateLockonEnemys()
		{
			return lockon.Where(x => Count - x.Value <= LockonTime)
				.Where(x => x.Key.Flag);
		}

		public void DrawAim()
		{
			_.Sprite.DrawCenter(aimTexture, aimPosition, Color.White, MathHelper.PiOver4, .5f);
			foreach (var item in EnumerateLockonEnemys().Select(x=>x.Key))
			{
				_.Sprite.DrawCenter(aimTexture, item.ContainerPosition, Color.Red, MathHelper.PiOver4, .5f);
			}
		}

		void Control()
		{
			UpdatePosition(Input.Horizontal * YawAngle, DefaultAccel - Input.Vertical * VariableAccel, Friction);
		}

		void Fire()
		{
			foreach (var item in EnumerateLockonEnemys().Select(x=>x.Key))
			{
				_.Audio.PlaySound("Rocket");
				_.World.Missiles.GetFirst().Set(this, item);
				animePlaying = true;
			}
		}

		public override void Draw()
		{
			var bone = animePlayer.GetSkinTransforms();
			foreach (var mesh in Model.Meshes)		
			{
				foreach (SkinnedEffect effect in mesh.Effects)
				{
					effect.SetBoneTransforms(bone);
					effect.View = _.World.Camera.View;
					effect.Projection = _.World.Camera.Projection;

				}
				mesh.Draw();
			}
		}
	}
}
