﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Masa.ScriptEngine;
using Microsoft.Xna.Framework.Graphics;
using Masa.Lib.XNA;

namespace Masa.Sphere.Chara
{
	public class Enemy : CharaBase
	{
		ScriptRunner script;
		Texture2D container;

		Vector3 containerPosition;
		public Vector2 ContainerPosition { get { return new Vector2(containerPosition.X, containerPosition.Y); } }

		float Accel { get; set; }
		float Friction { get; set; }
		float Yaw { get; set; }
		
		public Enemy()
		{
			SetModel("enemy");
			container = _.Game.Content.Load<Texture2D>("texture\\container");
		}

		public void Set(Vector3 pos, Vector3 crossVel, float accel, float fric, float yaw)
		{
			base.Set();
			SetPosition(pos);
			InitializeVelocityCross(accel, fric, crossVel);
			Accel = accel;
			Friction = fric;
			Yaw = yaw;
			Rotation = Vector3.Zero;
			
			script = GetScript("enemy");
		}

		void SetPosition(Vector3 dir)
		{
			Position = Vector3.Normalize(dir) * SphereRadius;
		}

		public void Hit()
		{
			Vanish();
		}

		//public override Vector3 FrontVector()
		//{
		//	return new Vector3(0, 0, 1);
		//}

		public override void Update()
		{
			base.Update();
			script.Update();
			UpdateContainer();
		}

		void UpdateContainer()
		{
			var c = _.World.Camera;
			var viewPort = _.Game.GraphicsDevice.Viewport;
			var p = Vector4.Transform(Position, GetWorld() * c.ViewProjection);
			containerPosition = new Vector3((p.X / p.W + 1) * viewPort.Width / 2, (1 - p.Y / p.W) * viewPort.Height / 2, p.Z / p.W);
		}

		public void DrawContainer()
		{
			_.Sprite.DrawCenter(container, ContainerPosition, Color.Red, 0, .5f, containerPosition.Z);
		}

		public override int GetHashCode()
		{
			return this.ListIndex;
		}

		public override bool Equals(object obj)
		{
			return obj is Enemy && (obj as Enemy).ListIndex == this.ListIndex;
		}
		
	}
}
