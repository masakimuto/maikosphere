﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Masa.Sphere.Chara
{
	public class Sphere : CharaBase
	{
		public Sphere()
		{
			SetModel("sphere");
			Scale = .48f;
		}

		public override Vector3 FrontVector()
		{
			return new Vector3(0, 0, 1);
		}

		public override Vector3 UpVector()
		{
			return new Vector3(0, 1, 0);
		}
	}
}
