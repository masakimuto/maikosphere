﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Masa.Lib.XNA;
using Masa.Lib;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Masa.ScriptEngine;

namespace Masa.Sphere.Chara
{
	public abstract class CharaBase : PoolObjectBase
	{
		[ScriptMember("position")]
		public Vector3 Position { get; protected set; }
		protected Vector3 Rotation;//yaw, pitch, roll
		protected int Count { get; set; }
		public Model Model { get; protected set; }
		[ScriptMember("scale")]
		protected float Scale;

		protected readonly float SphereRadius = 50;
		protected Vector3 Velocity { get; private set; }

		[ScriptMember("radius")]
		public float Radius { get; protected set; }

		[ScriptMember("ship")]
		public CharaBase Ship { get { return _.World.Ship; } }

		public CharaBase()
		{
			Scale = 1;
		}

		[ScriptMember("init_vel")]
		protected void InitializeVelocity(float accel, float friction, Vector3 dir)
		{
			float v = accel / friction;
			Velocity = Vector3.Normalize(dir) * v;
		}

		[ScriptMember("init_cross")]
		protected void InitializeVelocityCross(float accel, float friction, Vector3 crossDir)
		{
			InitializeVelocity(accel, friction, Vector3.Cross(crossDir, Position));
		}

		[ScriptMember("model")]
		protected void SetModel(string name)
		{
			Model = _.Game.Content.Load<Model>("model\\" + name);
		}

		protected bool IsCollision(CharaBase target)
		{
			return (Position - target.Position).LengthSquared() < MathUtil.Pow2(Radius + target.Radius);
		}

		protected ScriptRunner GetScript(string name)
		{
			return _.Script.GetScript(this, name);
		}

		public virtual void Update()
		{
			Count++;
		}

		public virtual void Draw()
		{
			Model.Draw(GetWorld(), _.World.Camera.View, _.World.Camera.Projection);
		}

		public virtual Matrix GetWorld()
		{
			return Matrix.CreateScale(Scale) * Matrix.CreateWorld(Position, -FrontVector(), UpVector());
			//return Matrix.CreateFromYawPitchRoll(Rotation.X, Rotation.Y, Rotation.Z) * Matrix.CreateScale(Scale) * Matrix.CreateTranslation(Position);
		}

		[ScriptMember("update")]
		protected void UpdatePosition(float accelAngle, float accel, float friction)
		{
			var last = Position;
			var vel = Velocity + GetAccel(accelAngle, accel);
			Position += vel * (1.0f - friction * vel.Length());
			Position = Vector3.Normalize(Position) * SphereRadius;
			Velocity = Position - last;
			Rotation.Y = MathUtil.Atan2(Position.Z, Position.Y);

			//Matrix.CreateWorld()
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="angle">速度方向に対する角度</param>
		/// <param name="speed">加速度大きさ</param>
		/// <returns></returns>
		Vector3 GetAccel(float angle, float speed)
		{
			var f = FrontVector();
			var h = LeftVector();
			return (f * MathUtil.Cos(angle) + h * MathUtil.Sin(angle)) * speed;
		}

		[ScriptMember("up")]
		public virtual Vector3 UpVector()
		{
			return Vector3.Normalize(Position);
		}

		[ScriptMember("front")]
		public virtual Vector3 FrontVector()
		{
			return Vector3.Normalize(Velocity);
		}

		[ScriptMember("left")]
		public Vector3 LeftVector()
		{
			return Vector3.Normalize(Vector3.Cross(Velocity, Position));
		}

		/// <summary>
		/// 原点を通り自機座標を法線とする平面上に投影したベクトル
		/// </summary>
		/// <returns></returns>
		Vector3 ShipPlaneProjectionVector(Vector3 v)
		{
			var s = _.World.Ship.Position;
			var k = -Vector3.Dot(s, v) / s.LengthSquared();
			return v + s * k;
		}

		/// <summary>
		/// 原点を通り自機座標を法線とする平面上に投影した座標ベクトル
		/// </summary>
		/// <returns></returns>
		public Vector3 ShipPlaneProjectionPositionVector()
		{
			return ShipPlaneProjectionVector(Position);
		}

		public Vector3 ShipPlaneProjectionVelocityVector()
		{
			return ShipPlaneProjectionVector(Velocity);
		}

		/// <summary>
		/// 自機から見て反対側にいるか
		/// </summary>
		/// <returns></returns>
		[ScriptMember("isbehind")]
		public bool IsBehind()
		{
			return Vector3.Dot(_.World.Ship.Position, Position) < 0;
		}

		[ScriptMember("vanish")]
		protected void Vanish()
		{
			base.Delete();
		}
		
	}
}
