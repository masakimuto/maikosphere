﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Masa.Lib.XNA;
using Masa.ScriptEngine;

namespace Masa.Sphere.Chara
{
	public class Missile : CharaBase
	{

		ScriptRunner script;

		[ScriptMember("target")]
		CharaBase Target { get; set; }
		[ScriptMember("parent")]
		CharaBase Parent { get; set; }

		public Missile()
			: base()
		{
		
		}

		public void Set(CharaBase parent, CharaBase target)
		{
			base.Set();
			Parent = parent;
			Position = parent.Position;
			Target = target;
			script = GetScript("missile");
		}

		public override void Update()
		{
			base.Update();
			script.Update();
			_.World.Particle.Set("missile", Position, 0, 0, 0);
			foreach (var item in _.World.Enemys.ActiveItems())
			{
				if (IsCollision(item))
				{
					Hit();
					item.Hit();
				}
			}
		}

		void Hit()
		{
			_.World.Particle.Set("bomb", Position);
			Delete();
			_.Audio.PlaySound("Hit");
		}
	}
}
