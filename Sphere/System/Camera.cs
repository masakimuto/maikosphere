﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Masa.ScriptEngine;

namespace Masa.Sphere.System
{
	public class Camera
	{
		[ScriptMember("position")]
		public Vector3 Position { get; private set; }
		[ScriptMember("target")]
		public Vector3 TargetPosition { get; private set; }
		[ScriptMember("up")]
		Vector3 UpVector;

		[ScriptMember("ship")]
		Chara.Ship Ship { get { return _.World.Ship; } }

		[ScriptMember("fov")]
		float Fov { get; set; }
		[ScriptMember("near")]
		float Near { get; set; }

		[ScriptMember("far")]
		float Far { get; set; }

		ScriptRunner script;


		public Camera()
		{
			script = _.Script.GetScript(this, "camera");
			UpdatePosition();
		}

		public void UpdatePosition()
		{
			script.Update();
			View = Matrix.CreateLookAt(Position, TargetPosition, UpVector);
			Projection = Matrix.CreatePerspectiveFieldOfView(Fov, _.Game.GraphicsDevice.Viewport.AspectRatio, Near, Far);
			ViewProjection = View * Projection;
		}

		public Matrix View { get; private set; }
		public Matrix Projection { get; private set; }
		public Matrix ViewProjection { get; private set; }
	}
}
