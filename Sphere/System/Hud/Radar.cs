﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Masa.Lib.XNA;
using Masa.Sphere.Chara;
using Masa.Lib;

namespace Masa.Sphere.System.Hud
{
	public class Radar : HudItemBase
	{
		Texture2D radar, mark, shot, compus;

		public Radar()
		{
			radar = LoadTexture("radar");
			mark = LoadTexture("radar_mark");
			shot = LoadTexture("radar_shot");
			compus = LoadTexture("radar_angle");
		}

		readonly float MarkScale = .5f;
		readonly int Size = 256;

		public override void Draw(Vector2 pos)
		{
			foreach (var item in EnumerateCharacters().Where(x=>x.IsBehind()))
			{
				DrawMark(item, 1, pos);
			}
			_.Sprite.Draw(radar, new Rectangle((int)pos.X, (int)pos.Y, Size, Size), Color.White * .5f);
			_.Sprite.DrawCenter(compus, new Vector2(Size * .5f) + pos, Color.White, GetCompusAngle(), 1);
			foreach (var item in EnumerateCharacters().Where(x=>!x.IsBehind()))
			{
				DrawMark(item, 1, pos);
			}
			DrawMark(mark, pos, Vector2.Zero, Color.White, 0);
		}

		float GetCompusAngle()
		{
			var s = _.World.Ship.Position;
			var v = Vector3.Up;
			var k = -Vector3.Dot(s, v) / s.LengthSquared();
			var a = v + s * k;
			return GetAngle(a, _.World.Ship.FrontVector(), _.World.Ship.Ship.UpVector());
		}

		IEnumerable<CharaBase> EnumerateCharacters()
		{
			return _.World.Missiles.ActiveItems().Cast<CharaBase>().Concat(_.World.Enemys.ActiveItems());
		}

		void DrawMark(CharaBase c, float alpha, Vector2 pos)
		{
			float dir = 0;
			dir = GetVelocityAngle(c);
			var tex = c is Chara.Enemy ? mark : shot;
			DrawMark(tex, pos, GetDrawPosition(c), c is Enemy ? Color.Red : Color.White, dir);
		}

		float GetVelocityAngle(CharaBase c)
		{
			var v = c.ShipPlaneProjectionVelocityVector();
			return GetAngle(v, _.World.Ship.FrontVector(), _.World.Ship.UpVector());
		}

		Vector2 GetDrawPosition(CharaBase c)
		{
			var p = c.ShipPlaneProjectionPositionVector();
			var r = p.Length() / c.Position.Length();
			var a = GetAngle(p, _.World.Ship.FrontVector(), _.World.Ship.UpVector());
			
			return MathUtilXNA.GetVector(Size * r * .5f, a - MathHelper.PiOver2);
		}

		float GetAngle(Vector3 v1, Vector3 v2, Vector3 upVector)
		{
			v1.Normalize();
			v2.Normalize();
			var cross = Vector3.Cross(v1, v2);
			var a = MathUtil.Atan2(cross.Length(), Vector3.Dot(v1, v2));
			if (Vector3.Dot(cross, upVector) < 0)
			{
				a *= -1;
			}
			return a;
		}

		void DrawMark(Texture2D texture, Vector2 basePos, Vector2 charaPos, Color color, float dir)
		{
			_.Sprite.DrawCenter(texture, new Vector2(Size * .5f) + basePos + charaPos, color, dir, MarkScale);
		}
	}
}
