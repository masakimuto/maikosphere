﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Masa.Lib.XNA;

namespace Masa.Sphere.System.Hud
{
	public class Wave : HudItemBase
	{
		SpriteFont font;

		public Wave()
		{
			font = _.Game.Content.Load<SpriteFont>("font\\timer");
		}



		public override void Draw(Vector2 pos)
		{
			_.Sprite.DrawStringRight(font, String.Format("Wave:{0}", _.World.Stage.Wave), pos, Color.White);
		}
	}
}
