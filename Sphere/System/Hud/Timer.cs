﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Masa.Lib.XNA;
using Microsoft.Xna.Framework;

namespace Masa.Sphere.System.Hud
{
	public class Timer : HudItemBase
	{
		SpriteFont font;

		public Timer()
		{
			font = _.Game.Content.Load<SpriteFont>("font\\timer");
		}

		public override void Draw(Vector2 pos)
		{
			var t = Math.Max(0, _.World.Time - _.World.Count);
			_.Sprite.DrawStringXCenter(font, string.Format("{0}:{1:00}", t / (60 * 60), (t / 60) % 60), pos, Color.White);
		}
	}
}
