﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Masa.Lib.XNA;

namespace Masa.Sphere.System
{
	public class Title
	{
		Texture2D logo;
		SpriteFont font;
		int count;

		public Title()
		{
			logo = _.Game.Content.Load<Texture2D>("texture\\title");
			font = _.Game.Content.Load<SpriteFont>("font\\timer");
		}

		public void Update()
		{
			count++;
			if (_.Input.MouseState.Left.JustPush)
			{
				_.World.StartGame();
			}
		}

	

		public void Draw()
		{
			_.Sprite.Begin();
			_.Sprite.DrawCenter(logo, new Vector2(640, 200), Color.White, 0, 1);
			var color = Color.White * (Masa.Lib.Utility.Vibrate(count, 60, .3f) + 0.7f);
			_.Sprite.DrawStringCenter(font, "Click to Start", new Vector2(640, 600), color);
			_.Sprite.End();
		}
	}
}
