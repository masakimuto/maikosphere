﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Masa.Lib;
using Microsoft.Xna.Framework;

namespace Masa.Sphere.System
{
	public class CharaManagerBase<T> : PoolObjectManagerBase<T> where T: Chara.CharaBase, new()
	{
		public CharaManagerBase()
		:base(64)
		{
		}

		public virtual void Update()
		{
			foreach (var item in ActiveItems())
			{
				item.Update();
			}
		}

		public virtual void Draw()
		{
			foreach (var item in ActiveItems())
			{
				item.Draw();
			}
		}

		public void Clear()
		{
			this.DeleteAll();
		}

		public override T GetFirst()
		{
			return GetFirstUnusedItemWithExtend();
		}

	}
}
