﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Masa.Sphere.System
{
	public class GameWorld
	{
		public Camera Camera { get; private set; }
		public Chara.Ship Ship { get; private set; }
		Chara.Sphere sphere;
		public CharaManagerBase<Chara.Enemy> Enemys { get; private set; }
		public CharaManagerBase<Chara.Missile> Missiles { get; private set; }
		public HudManager Hud { get; private set; }
		public General.ParticleManager Particle { get; private set; }
		public StageManager Stage { get; private set; }

		public readonly int Time = 60 * 60 * 3;
		public int Count { get; private set; }


		Title title;
		enum GameState
		{
			Title,
			Running,
			Gameover,
		}
		GameState state;
		
		public GameWorld()
		{
			_.Audio.PlayLockon(false);
			_.World = this;
			title = new Title();
			sphere = new Chara.Sphere();
			Ship = new Chara.Ship();
			Camera = new Camera();
			Hud = new HudManager();
			Enemys = new CharaManagerBase<Chara.Enemy>();
			Missiles = new CharaManagerBase<Chara.Missile>();
			Stage = new StageManager();
			Particle = new General.ParticleManager(Camera.Projection);
			_.Audio.PlayBGM(false);
		}

		public void Update()
		{
			if (state == GameState.Title)
			{
				title.Update();
			}
			else
			{
				if (state == GameState.Gameover)
				{
					UpdateGameOver();
				}
				if (state == GameState.Running)
				{
					Count++;
					_.Input.ClampMousePosition(_.Game.Window.ClientBounds.Width, _.Game.Window.ClientBounds.Height);
					_.Input.UpdateGamePlayFromControl();
				}
				Ship.Update();
				Camera.UpdatePosition();
				Enemys.Update();
				Missiles.Update();
				Particle.Update();
				Hud.Update();
				if (state == GameState.Running)
				{
					Stage.Update();
				}
				if (Count >= Time)
				{
					Exit();
				}
				
			}
		}

		void UpdateGameOver()
		{
			if (_.Input.MouseState.Left.JustPush)
			{
				_.Game.CreateWorld();
			}
		}

		public void StartGame()
		{
			state = GameState.Running;
			Enemys.Clear();
			Missiles.Clear();
			_.Audio.PlayBGM(true);
			_.Audio.PlayLockon(false);
		}


		public void Draw()
		{
			var view = Camera.View;
			var proj = Camera.Projection;
			SetModelDrawState();
			sphere.Draw();
			Ship.Draw();
			if (state != GameState.Title)
			{
				Enemys.Draw();
				Missiles.Draw();
				DrawTargetContainers();
			}
			
			Particle.Draw(view);
			Hud.Draw();
			if (state == GameState.Title)
			{
				title.Draw();
			}
		}

		void DrawTargetContainers()
		{
			_.Sprite.Begin(SpriteSortMode.Deferred, null, null, DepthStencilState.DepthRead, null);
			foreach (var item in Enemys.ActiveItems())
			{
				item.DrawContainer();
			}
			Ship.DrawAim();
			_.Sprite.End();
		}

		void SetModelDrawState()
		{
			_.Game.GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
			_.Game.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
		}

	
		public void Exit()
		{
			state = GameState.Gameover;
			_.Audio.PlayBGM(false);
			_.Audio.PlayLockon(false);
		}
	}
}
