﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Masa.ScriptEngine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Masa.Sphere.System
{
	public abstract class HudItemBase
	{
		protected int Count { get; private set; }
		

		public virtual void Update()
		{
			Count++;
		}
		public abstract void Draw(Vector2 pos);

		protected Texture2D LoadTexture(string name)
		{
			return _.Game.Content.Load<Texture2D>("texture\\" + name);
		}
	}

	public class HudManager
	{
		List<HudItemBase> items;
		ScriptRunner script;

		public HudManager()
		{
			items = new List<HudItemBase>()
			{
				new Hud.Radar(),
				new Hud.Timer(),
				new Hud.Wave(),
			};
			script = _.Script.GetScript(this, "hud");
		}

		public void Update()
		{
			foreach (var item in items)
			{
				item.Update();
			}
		}

		public void Draw()
		{
			_.Sprite.Begin();
			script.Update();
			_.Sprite.End();
		}

		[ScriptMember("draw")]
		void Draw(int index, Vector2 pos)
		{
			items[index].Draw(pos);
		}
	}
}
