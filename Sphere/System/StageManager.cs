﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Masa.ScriptEngine;
using Microsoft.Xna.Framework;

namespace Masa.Sphere.System
{
	public class StageManager
	{
		ScriptRunner script;

		public int Wave
		{
			get { return (int)script.State; }
		}

		public StageManager()
		{
			script = _.Script.GetScript(this, "stage");
			//script.Update();
		}

		[ScriptMember("set")]
		public void Set(Vector3 pos, Vector3 cross, float accel, float fric, float yaw)
		{
			_.World.Enemys.GetFirst().Set(pos, cross, accel, fric, yaw);
		}

		public void Update()
		{
			if (!_.World.Enemys.ActiveItems().Any())
			{
				script.State++;
				script.Update();
			}
		}

		[ScriptMember("exit")]
		void Exit()
		{
			_.World.Exit();
		}
	}
}
