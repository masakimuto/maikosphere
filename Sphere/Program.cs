﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masa.Sphere
{
	public static class _
	{
		public static SphereGame Game;
		public static Masa.Lib.XNA.Input.InputManager Input;
		public static Microsoft.Xna.Framework.Graphics.SpriteBatch Sprite;
		public static System.GameWorld World;
		public static General.ScriptManager Script;
		public static General.AudioManager Audio;
	}

	class Program
	{
		static void Main()
		{
			using (var game = new SphereGame())
			{
				game.Run();
			}
		}
	}
}
