﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Masa.ScriptEngine;


namespace Masa.Sphere.General
{
	public class ScriptManager : Masa.ScriptEngine.ScriptManager
	{
		public ScriptManager()
			: base("Content\\script")
		{
			LoadFromDirectory("ship", typeof(Chara.Ship));
			LoadFromDirectory("camera", typeof(System.Camera));
			LoadFromDirectory("enemy", typeof(Chara.Enemy));
			LoadFromDirectory("shot", typeof(Chara.Missile));
			LoadFromDirectory("hud", typeof(System.HudManager));
			LoadFromDirectory("stage", typeof(System.StageManager));

			OutputDocumentsByClass("doc");
		}
	}
}
