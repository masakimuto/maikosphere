﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Masa.ParticleEngine;

namespace Masa.Sphere.General
{
	public class ParticleManager : ScriptEffectManager
	{

		public ParticleManager(Matrix projection)
			: base(_.Script, _.Game.GraphicsDevice, _.Game.Content.Load<Effect>("effect\\particle"),
				ParticleMode.ThreeD, projection,
				new Vector2(_.Game.GraphicsDevice.Viewport.Width, _.Game.GraphicsDevice.Viewport.Height),
				ParticleManagerInitializerManager.LoadPMIFromFile("content\\particle.efprj", s => _.Game.Content.Load<Texture2D>("particle\\" + global::System.IO.Path.GetFileNameWithoutExtension(s))),
				null)
		{
			LoadDirectoryScript("effect");

		}


	}
}
