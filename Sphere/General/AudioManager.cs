﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Audio;

namespace Masa.Sphere.General
{
	public class AudioManager : IDisposable
	{
		AudioEngine engine;
		SoundBank sound;
		WaveBank wSound;
		Cue lockonCue, bgmCue;

		public AudioManager()
		{
			engine = new AudioEngine("content\\audio\\audio.xgs");
			sound = new SoundBank(engine, "content\\audio\\sound.xsb");
			wSound = new WaveBank(engine, "content\\audio\\sound.xwb");
			lockonCue = sound.GetCue("Sine");
			bgmCue = sound.GetCue("bgm");
		}

		public void PlaySound(string name)
		{
			sound.PlayCue(name);
		}

		public void PlayLockon(bool isPlay)
		{
			PlayCue(isPlay, lockonCue);
		}

		void PlayCue(bool isPlay, Cue cue)
		{
			if (isPlay)
			{
				if (cue.IsPaused)
				{
					cue.Resume();
				}
				else if (cue.IsPrepared)
				{
					cue.Play();
				}
			}
			else
			{
				if (!cue.IsPrepared && !cue.IsPaused)
				{
					cue.Pause();
				}
			}
		}

		public void PlayBGM(bool isPlay)
		{
			PlayCue(isPlay, bgmCue);
		}

		public void Update()
		{
			engine.Update();
		}

		public void Dispose()
		{
			if (engine != null)
			{
				engine.Dispose();
				engine = null;
			}
		}
	}
}
