varg ac
varg fric
varg yaw
var lf
var ll
var lr
label init
	model missile
	scale = 0.008
	ac = 0.007
	fric = 0.01
	yaw = (PI * 0.3)
	radius = 20 * scale
	init_vel ac fric (parent.front)
;if(((position + left - target.position).len2 > (position - left - target.position).len2) ^ ((dot3 position target.position) < 0))

;if((dot3 (position - target.position) front) < 0)
lf = (position - target.position).len2
ll = (position - left - target.position).len2
lr = (position + left - target.position).len2

;if(lf < ll & lf < lr)
	;update 0 ac fric
;else
if(lf > ll)
	update (-yaw) ac fric
else
	update yaw ac fric
ac += 0.0002
if (scount > 300)
	vanish
