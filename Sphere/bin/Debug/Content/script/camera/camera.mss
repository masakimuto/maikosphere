var uv
var fv
label init
	near = 0.1
	far = 1000
	fov = PI / 4
;	goto 1

fv = ship.front
uv = ship.up
state 0
	position = ship.position + uv * 10 + fv * -8
	target = ship.position + uv * 0 + fv * 4
	up = uv
state 1
	up = fv
	target = ship.position
	position = ship.position + uv * 10
	fov = PI * 0.6
	goto 2
