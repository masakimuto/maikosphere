﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System.Xml.Linq;
using System.IO;
using Masa.Lib.XNA.Input;
using MSKey = Microsoft.Xna.Framework.Input.Keys;

namespace Masa.Sphere
{
	

	public class SphereGame : Game
	{
		GraphicsDeviceManager graphics;
		public static readonly int Width = 1280;
		public static readonly int Height = 720;
		

		public SphereGame()
		{
			_.Game = this;
			graphics = new GraphicsDeviceManager(this)
			{
				PreferredBackBufferWidth = Width,
				PreferredBackBufferHeight = Height
			};
			Window.Title = "球面上のまい子";
		}

		protected override void LoadContent()
		{
			base.LoadContent();
			Content.RootDirectory = "Content";
			_.Sprite = new SpriteBatch(GraphicsDevice);
			_.Input = new Lib.XNA.Input.InputManager(this, Lib.XNA.Input.ActiveDevice.Keyboard | Lib.XNA.Input.ActiveDevice.Mouse);
			LoadKeyConfig();
			_.Audio = new General.AudioManager();
			CreateWorld();
		}

		void LoadKeyConfig()
		{
			Stream file = null;
			try
			{
				file = File.OpenRead("config.xml");
				var doc = XDocument.Load(file);
				_.Input.KeyConfig = KeyboardConfig.FromXElement(doc.Element(_.Input.KeyConfig.XElementName));
			}
			catch
			{

			}
			finally
			{
				if (file != null)
				{
					file.Dispose();
				}
			}
			try
			{
				file = File.OpenWrite("config.xml");
				new XDocument(_.Input.KeyConfig.ToXElement()).Save(file);
			}
			catch
			{

			}
			finally
			{
				if (file != null)
				{
					file.Dispose();
				}
			}
			
			//var doc = XDocument.Load()
			if (File.Exists("wasd"))
			{
				var keys = new[]{ MSKey.W, MSKey.S, MSKey.A, MSKey.D };
				for (int i = 0; i < KeyboardConfig.ArrowArray.Length; i++)
				{
					KeyboardConfig.ArrowArray[i] = keys[i];
				}

			}
		}


		public void CreateWorld()
		{
			_.Script = new General.ScriptManager();
			_.World = new System.GameWorld();
		}

		protected override void Update(GameTime gameTime)
		{
			if (!IsActive)
			{
				return;
			}
			base.Update(gameTime);
			_.Input.Update();
			_.Audio.Update();
			if (_.Input.ControlState.Esc.JustPush)
			{
				CreateWorld();
			}
			_.World.Update();
		}
		

		protected override void Draw(GameTime gameTime)
		{
			GraphicsDevice.Clear(ClearOptions.Target | ClearOptions.DepthBuffer, Color.CornflowerBlue, 1, 0);
			_.World.Draw();
			base.Draw(gameTime);
		}

		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
			if (_.Input != null)
			{
				_.Input.Dispose();
			}
			if (_.Audio != null)
			{
				_.Audio.Dispose();
			}
		}
	}
}
